﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task17;

namespace WindowsFormsTask17part2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            //Testing SQL Basics

            SQLiteConnection conn = new SQLiteConnection("DataSource = name.db; Version = 3; New = True; Compress =  True; ");

            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            SQLiteCommand sqlite_cmd =conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTOSampleTable(Col1, Col2) VALUES('Test Text ',1); ";
            sqlite_cmd.ExecuteNonQuery();

            SQLiteDataReader sqlite_datareader;
            //SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROMSampleTable";
            sqlite_datareader = sqlite_cmd.ExecuteReader();

        }
    }
}
