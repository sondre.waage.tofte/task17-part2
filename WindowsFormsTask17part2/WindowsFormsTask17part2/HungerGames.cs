﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task17;

namespace WindowsFormsTask17part2
{
    class HungerGames
    {
        
        public static void StartFight(Character warrior1, Character warrior2)
        {
            while (true)
            {
                if ((GetAttackResult(warrior1, warrior2)) == "Game Over")
                {
                    Console.WriteLine("Game Over");
                    break;
                }
                if ((GetAttackResult(warrior2, warrior1)) == "Game Over")
                {
                    Console.WriteLine("Game Over");
                    break;
                }

            }
        }
        //Get attack result
        // Warrior 1 and Warrior 2
        public static string GetAttackResult(Character warriorA, Character warriorB)
        {
            Random rnd = new Random();
            int warAAttackAmmount = rnd.Next(1, warriorA.Energy);
            int warBBlockAmmount = rnd.Next(1, warriorB.ArmorRating);
            int dmgdone = warAAttackAmmount - warBBlockAmmount;

            if (dmgdone > 0)
            {
                warriorB.HealthPoints = warriorB.HealthPoints - dmgdone;
            }
            else dmgdone = 0;

            Console.WriteLine("{0} did {1} damage to {2}", warriorA.Name, dmgdone, warriorB.Name);

            Console.WriteLine("{0} got {1} health left", warriorB.Name, warriorB.HealthPoints);
            if (warriorA.HealthPoints < 1)
            {
                Console.WriteLine("{0} is dead, {1} has won", warriorA.Name, warriorB.Name);
                return "Game Over";
            }
            else if (warriorB.HealthPoints < 1)
            {
                Console.WriteLine("{0} is dead, {1} has won", warriorB.Name, warriorA.Name);
                return "Game Over";
            }
            else return "Fight again";
            
        }
    }
}