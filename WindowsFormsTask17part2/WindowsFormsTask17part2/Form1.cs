﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task17;

namespace WindowsFormsTask17part2
{ 
    public partial class Form1 : Form
    {
        static Warrior Attacker1 = new Warrior("Gandalf", 100, 100, 20);
        static Thief Attacker2 = new Thief("Gandalf", 100, 100, 20);
        static Wizard Attacker3 = new WhiteWizard("Gandalf", 100, 100, 20);
        static Wizard Attacker4 = new WhiteWizard("Gandalf", 100, 100, 20);
        static Warrior Defender1 = new Warrior("Nick", 150, 100, 80);
        static Thief Defender2 = new Thief("Nick", 150, 100, 80);
        static Wizard Defender3 = new WhiteWizard("Nick", 150, 100, 80);
        static Wizard Defender4 = new GreyWizard("Nick", 150, 100, 80);
        string selectedname1 = "name1";
        string selectedname2 = "name2";
        string selectedclass1 = "class1";
        string selectedclass2 = "class2";
        int class1 = 0;
        int class2 = 0;
        

        public Form1()
        {
                      InitializeComponent();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            selectedname1 = textBox1.Text;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //fjern denne senere
            int numberoffighters = 2;
            if (numberoffighters>1)
            {
                MessageBox.Show("You have started the HungerGames");
                HungerGames.StartFight(Attacker1, Defender1);
                //MessageBox.Show(message);
            }
            else
            {
                MessageBox.Show("You need more fighters");
            }
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "Warrior";
            class1 = 1;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You have created a defender");
            if (class2 == 1)
            {
                Defender1.Name = selectedname2;
            }
            else if (class2 == 2)
            {
                Defender2.Name = selectedname2;
            }
            else if (class2 == 3)
            {
                Defender3.Name = selectedname2;
            }
            else if (class2 == 4)
            {
                Defender4.Name = selectedname2;
            }
            else
            {
                MessageBox.Show("Select a class");
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You have created an attacker");
            if (class1 == 1)
            {
                Attacker1.Name = selectedname1;
            }
            else if (class1 == 2)
            {
                Attacker2.Name = selectedname1;
            }
            else if (class1 == 3)
            {
                Attacker3.Name = selectedname1;
            }
            else if (class1 == 4)
            {
                Attacker4.Name = selectedname1;
            }
            else
            {
                MessageBox.Show("Select a class");
            }
        }
        private void Button3_Click(object sender, EventArgs e)
        {

        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "Thief";
            class1 = 2;
        }

        private void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "White Wizard";
            class1 = 3;
        }

        private void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "Grey Wizard";
            class1 = 4;

        }

        private void RadioButton8_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass2 = "Warrior";
            class2 = 1;
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            selectedname2 = textBox2.Text;
        }

        private void RadioButton7_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass2 = "Thief";
            class2 = 2;
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            if (class1 == 1)
            {
                MessageBox.Show(Attacker1.PrintSummary());
            }
            else if (class1 == 2)
            {
                MessageBox.Show(Attacker2.PrintSummary());
            }
            else if (class1 == 3)
            {
                MessageBox.Show(Attacker3.PrintSummary());
            }
            else if (class1 == 4)
            {
                MessageBox.Show(Attacker4.PrintSummary());
            }
            else
            {
                MessageBox.Show("Can not find the Attackerclass");
            }
        }

        private void RadioButton5_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass2 = "Grey Wizard";
            class2 = 4;
        }

        private void RadioButton6_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass2 = "White Wizard";
            class2 = 3;
        }

        private void Button3_Click_1(object sender, EventArgs e)
        {
            if (class2==1)
            {
                MessageBox.Show(Defender1.PrintSummary());
            }
            else if (class2 == 2)
            {
                MessageBox.Show(Defender2.PrintSummary());
            }
            else if (class2 == 3)
            {
                MessageBox.Show(Defender3.PrintSummary());
            }
            else if (class2 == 4)
            {
                MessageBox.Show(Defender4.PrintSummary());
            }
            else
            {
                MessageBox.Show("Can not find the defenderclass");
            }
        }
    }
}
